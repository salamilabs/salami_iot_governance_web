  'use strict';

/**
 * @ngdoc filter
 * @name salamiGovernanceApp.filter:kitchenStatus
 * @function
 * @description
 * # kitchenStatus
 * Filter in the salamiGovernanceApp.
 */
angular.module('salamiGovernanceApp')
  .filter('statusFilter', function () {
    return function (input) {
      if(input === 1) {
        return "Enabled";
      } else {
        return "Disabled";
      }
    };
  });
