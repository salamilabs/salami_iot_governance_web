'use strict';

/**
 * @ngdoc service
 * @name salamiGovernanceApp.Messages
 * @description
 * # Messages
 * Service in the salamiGovernanceApp.
 */
angular.module('salamiGovernanceApp')
  .service('Messages', function () {
    this.messages = [];

    this.addMessage = function (alertType, message) {
      if (this.messages.length == 3) {
        this.messages = [];
      }
      this.messages.push({type: alertType, msg: message});
    };

    this.removeMessage = function (index) {
      this.messages.splice(index, 1);
    };
  });
