'use strict';

/**
 * @ngdoc service
 * @name salamiGovernanceApp.Util
 * @description
 * # Util
 * Service in the salamiGovernanceApp.
 */
angular.module('salamiGovernanceApp')
  .service('Util', function (UserData) {
    this.getAuthorizationHeader = function () {
      var token = UserData.token;
      var authHeader = {};

      if (token != null && token != undefined && token !== "") {
        authHeader['Authorization'] = "Bearer " + token;
      }
      return authHeader;
    };

    this.getAuthorizationParameter = function () {
      return {oauth_consumer_key: UserData.token}
    }
  });
