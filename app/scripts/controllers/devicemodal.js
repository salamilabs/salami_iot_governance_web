'use strict';

/**
 * @ngdoc function
 * @name salamiGovernanceApp.controller:DevicemodalCtrl
 * @description
 * # DevicemodalCtrl
 * Controller of the salamiGovernanceApp
 */
angular.module('salamiGovernanceApp')
  .controller('DevicemodalCtrl', function ($scope, $uibModalInstance, device) {
    $scope.device = device;

    $scope.ok = function () {
      $uibModalInstance.close($scope.device);
    };

    $scope.cancel = function () {
      $uibModalInstance.dismiss('cancel');
    };
  });
