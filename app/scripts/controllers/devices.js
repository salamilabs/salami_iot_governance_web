'use strict';

/**
 * @ngdoc function
 * @name salamiGovernanceApp.controller:DevicesCtrl
 * @description
 * # DevicesCtrl
 * Controller of the salamiGovernanceApp
 */
angular.module('salamiGovernanceApp')
  .controller('DevicesCtrl', function ($scope, $log, $uibModal, Resource, Messages) {
    loadDevices();

    // Delete Modal
    $scope.openDeviceDeleteModal = function (device) {
      var modalInstance = $uibModal.open({
        animation: true,
        templateUrl: 'devicedeletemodalcontent.html',
        controller: 'DevicemodalCtrl',
        size: '',
        resolve: {
          device: function () {
            return device;
          }
        }
      });

      modalInstance.result.then(function (device) {
        device.deleted = true;
        device.passport = undefined;
        Resource.deleteDevice(device).then(function () {
          loadDevices();
        }, function (error) {
          $log.error("Error - Cannot delete the device", error);
          Messages.addMessage('danger', 'Error - Cannot delete the device');
        });
      }, function () {
        $log.info('Modal dismissed at: ' + new Date());
      });
    };

    function loadDevices() {
      Resource.getDevices().then(function (devices) {
        $scope.devices = devices;
      }, function (error) {
        $log.error("Error - Cannot load data from server", error);
        Messages.addMessage('danger', 'Error - Cannot load data from server');
      });
    }
  });
