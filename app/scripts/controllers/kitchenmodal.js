'use strict';

/**
 * @ngdoc function
 * @name salamiGovernanceApp.controller:KitchenmodalCtrl
 * @description
 * # KitchenmodalCtrl
 * Controller of the salamiGovernanceApp
 */
angular.module('salamiGovernanceApp')
  .controller('KitchenmodalCtrl', function ($scope, $uibModalInstance, kitchen) {
    $scope.kitchen = kitchen;

    $scope.ok = function () {
      $uibModalInstance.close($scope.kitchen);
    };

    $scope.cancel = function () {
      $uibModalInstance.dismiss('cancel');
    };
  });
