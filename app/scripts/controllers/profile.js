'use strict';

/**
 * @ngdoc function
 * @name salamiGovernanceApp.controller:ProfileCtrl
 * @description
 * # ProfileCtrl
 * Controller of the salamiGovernanceApp
 */
angular.module('salamiGovernanceApp')
  .controller('ProfileCtrl', function ($scope, $log, Resource, Messages) {
    Resource.getUser().then(function (users) {
      $scope.user = users[0];
    }, function (error) {
      $log.error("Error - Cannot load data from server", error);
      Messages.addMessage('danger', 'Error - Cannot load data from server');
    });

    $scope.updateSettings = function() {
      Resource.updateUser($scope.user).then(function () {
        Messages.addMessage('success', 'Account settings correctly updated');
      }, function (error) {
        $log.error("Error - Cannot update user account settings", error);
        Messages.addMessage('danger', 'Error - Cannot update user account settings');
      });
    };

    $scope.updatePassword = function () {

    }
  });
