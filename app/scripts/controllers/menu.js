'use strict';

/**
 * @ngdoc function
 * @name salamiGovernanceApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the salamiGovernanceApp
 */
angular.module('salamiGovernanceApp')
  .controller('MenuCtrl', function ($scope, $location, Login, UserData) {
    $scope.$watch(function () {
      return UserData.username;
    }, function () {
      $scope.username = UserData.username;
    });

    $scope.logout = function () {
      Login.logout();
    };

    $scope.isActive = function (viewLocation) {
      return viewLocation === $location.path();
    };

  });
