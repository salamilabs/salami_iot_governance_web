'use strict';

/**
 * @ngdoc function
 * @name salamiGovernanceApp.controller:HomeCtrl
 * @description
 * # HomeCtrl
 * Controller of the salamiGovernanceApp
 */
angular.module('salamiGovernanceApp')
  .controller('HomeCtrl', function ($scope, $log, $location, $uibModal, Resource, UserData, Messages) {
    loadKitchens();

    $scope.removeKitchen = function (kitchen) {
      $scope.openKitchenDeleteModal(kitchen);
    };

    // Kitchen Modal
    $scope.openKitchenModal = function (kitchen) {
      $scope.updating = kitchen != undefined;
      var modalInstance = $uibModal.open({
        animation: true,
        templateUrl: 'kitchenmodalcontent.html',
        controller: 'KitchenmodalCtrl',
        size: '',
        resolve: {
          kitchen: function () {
            return kitchen;
          }
        }
      });

      modalInstance.result.then(function (kitchen) {
        if ($scope.updating) {
          Resource.updateKitchen(kitchen).then(function () {
            Messages.addMessage('success', 'Kitchen correctly updated');
          }, function (error) {
            $log.error("Error - Cannot save kitchen", error);
            Messages.addMessage('danger', 'Error - Cannot update kitchen');
          });
        } else {
          kitchen.owner = UserData.uri;
          Resource.saveKitchen(kitchen).then(function () {
            Messages.addMessage('success', 'Kitchen correctly created');
            loadKitchens();
          }, function (error) {
            $log.error("Error - Cannot create kitchen", error);
            Messages.addMessage('danger', 'Error - Cannot create kitchen');
          });
        }
      }, function () {
        $log.debug('Modal dismissed at: ' + new Date());
      });
    };

    // Delete Modal
    $scope.openKitchenDeleteModal = function (kitchen) {
      var modalInstance = $uibModal.open({
        animation: true,
        templateUrl: 'kitchendeletemodalcontent.html',
        controller: 'KitchenmodalCtrl',
        size: '',
        resolve: {
          kitchen: function () {
            return kitchen;
          }
        }
      });

      modalInstance.result.then(function (kitchen) {
        kitchen.deleted = true;
        Resource.deleteKitchen(kitchen).then(function () {
          loadKitchens();
        }, function (error) {
          $log.error("Error - Cannot delete kitchen", error);
          Messages.addMessage('danger', 'Error - Cannot delete kitchen');
        });
      }, function () {
        $log.debug('Modal dismissed at: ' + new Date());
      });
    };

    function loadKitchens() {
      Resource.getKitchens().then(function (kitchens) {
        $scope.kitchens = kitchens;
        $log.info("Kitchen List: ", $scope.kitchens);
      }, function (error) {
        $log.error("Error - Cannot load data from server", error);
        Messages.addMessage('danger', 'Error - Cannot load data from server');
      });
    }

  });
