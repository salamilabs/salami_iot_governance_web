'use strict';

/**
 * @ngdoc function
 * @name salamiGovernanceApp.controller:KeysCtrl
 * @description
 * # KeysCtrl
 * Controller of the salamiGovernanceApp
 */
angular.module('salamiGovernanceApp')
  .controller('KeysCtrl', function ($scope, $log, $uibModal, Resource, Messages) {
    loadKeys();

    $scope.removeKey = function (key) {
      $scope.openKeyDeleteModal(key);
    };

    $scope.enableKey = function (key) {
      key.status = 1;
      Resource.updateKey(key).then(function () {
        loadKeys();
      }, function (error) {
        $log.error("Error - Cannot enable the key", error);
        Messages.addMessage('danger', 'Error - Cannot enable the key');
      });
    };

    $scope.disableKey = function (key) {
      key.status = 0;
      Resource.updateKey(key).then(function () {
        loadKeys();
      }, function (error) {
        $log.error("Error - Cannot disable the key", error);
        Messages.addMessage('danger', 'Error - Cannot disable the key');
      });
    };

    // Keys Modal
    $scope.openKeyModal = function (key) {
      $scope.updating = key != undefined;
      var modalInstance = $uibModal.open({
        animation: true,
        templateUrl: 'keysmodalcontent.html',
        controller: 'KeymodalCtrl',
        size: '',
        resolve: {
          key: function () {
            return key;
          }
        }
      });

      modalInstance.result.then(function (key) {
        Resource.saveKey(key).then(function () {
          Messages.addMessage('success', 'Key correctly created');
          loadKeys();
        }, function (error) {
          $log.error("Error - Cannot create the key", error);
          Messages.addMessage('danger', 'Error - Cannot create the key');
        });

      }, function () {
        $log.debug('Modal dismissed at: ' + new Date());
      });
    };

    // Delete Modal
    $scope.openKeyDeleteModal = function (key) {
      var modalInstance = $uibModal.open({
        animation: true,
        templateUrl: 'keydeletemodalcontent.html',
        controller: 'KeymodalCtrl',
        size: '',
        resolve: {
          key: function () {
            return key;
          }
        }
      });

      modalInstance.result.then(function (key) {
        key.deleted = true;
        Resource.deleteKey(key).then(function () {
          loadKeys();
        }, function (error) {
          $log.error("Error - Cannot delete the key", error);
          Messages.addMessage('danger', 'Error - Cannot delete the key');
        });
      }, function () {
        $log.info('Modal dismissed at: ' + new Date());
      });
    };

    function loadKeys() {
      Resource.getKeys().then(function (keys) {
        $scope.keys = keys;
        $log.info("Keys List: ", $scope.keys);
      }, function (error) {
        $log.error("Error - Cannot load data from server", error);
        Messages.addMessage('danger', 'Error - Cannot load data from server');
      });
    }

  });
