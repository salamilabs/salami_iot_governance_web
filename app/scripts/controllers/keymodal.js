'use strict';

/**
 * @ngdoc function
 * @name salamiGovernanceApp.controller:KeymodalCtrl
 * @description
 * # KeymodalCtrl
 * Controller of the salamiGovernanceApp
 */
angular.module('salamiGovernanceApp')
  .controller('KeymodalCtrl', function ($scope, $uibModalInstance, Resource, key, Messages) {
    $scope.key = key;

    Resource.getKitchens().then(function (kitchens) {
      $scope.kitchens = kitchens;
    }, function (error) {
      $log.error("Error - Cannot load data from server", error);
      Messages.addMessage('danger', 'Error - Cannot load data from server');
    });

    $scope.ok = function () {
      $uibModalInstance.close($scope.key);
    };

    $scope.cancel = function () {
      $uibModalInstance.dismiss('cancel');
    };
  });
