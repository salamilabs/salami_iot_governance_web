'use strict';

/**
 * @ngdoc overview
 * @name salamiGovernanceApp
 * @description
 * # salamiGovernanceApp
 *
 * Main module of the application.
 */
angular
  .module('salamiGovernanceApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'restangular',
    'ui.bootstrap',
    'ui.bootstrap.showErrors'
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/login', {
        templateUrl: 'views/login.html',
        controller: 'LoginCtrl',
        controllerAs: 'login'
      })
      .when('/', {
        templateUrl: 'views/home.html',
        controller: 'HomeCtrl',
        controllerAs: 'home'
      })
      .when('/devices', {
        templateUrl: 'views/devices.html',
        controller: 'DevicesCtrl',
        controllerAs: 'devices'
      })
      .when('/profile', {
        templateUrl: 'views/profile.html',
        controller: 'ProfileCtrl',
        controllerAs: 'profile'
      })
      .otherwise({
        redirectTo: '/'
      });
  }).config(function (RestangularProvider, Config) {
  RestangularProvider.setBaseUrl(Config.apiBaseUrl);
  RestangularProvider.setRequestSuffix("/");
  RestangularProvider.setExtraFields(['name']);
  RestangularProvider.setDefaultRequestParams('get', {format: 'json', 't': new Date().getTime()});
  RestangularProvider.setResponseExtractor(function (response, operation) {
    return response.objects;
  });
}).run(function ($rootScope, $location, UserData) {
  $rootScope.$on("$routeChangeStart", function (event, next, current) {
    if (sessionStorage.loggedUser !== null && sessionStorage.loggedUser !== undefined && sessionStorage.loggedUser !== "") {
      var loggedUser = JSON.parse(sessionStorage.loggedUser);
      var token = loggedUser.token;
      if (token == null || token == undefined || token === "") {
        if (next.templateUrl == "views/login.html") {
        } else {
          $location.path("/login");
        }
      } else {
        UserData.init(loggedUser);
      }
    } else {
      $location.path("/login");
    }

  });
});
