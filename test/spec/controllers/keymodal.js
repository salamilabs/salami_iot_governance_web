'use strict';

describe('Controller: KeymodalCtrl', function () {

  // load the controller's module
  beforeEach(module('salamiGovernanceApp'));

  var KeymodalCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    KeymodalCtrl = $controller('KeymodalCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(KeymodalCtrl.awesomeThings.length).toBe(3);
  });
});
