'use strict';

describe('Controller: KeysCtrl', function () {

  // load the controller's module
  beforeEach(module('salamiGovernanceApp'));

  var KeysCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    KeysCtrl = $controller('KeysCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(KeysCtrl.awesomeThings.length).toBe(3);
  });
});
