'use strict';

describe('Filter: kitchenStatus', function () {

  // load the filter's module
  beforeEach(module('salamiGovernanceApp'));

  // initialize a new instance of the filter before each test
  var kitchenStatus;
  beforeEach(inject(function ($filter) {
    kitchenStatus = $filter('kitchenStatus');
  }));

  it('should return the input prefixed with "kitchenStatus filter:"', function () {
    var text = 'angularjs';
    expect(kitchenStatus(text)).toBe('kitchenStatus filter: ' + text);
  });

});
